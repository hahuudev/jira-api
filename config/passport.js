const passport = require("passport");
const GoogleStrategy = require("passport-google-oauth20").Strategy;
const OutlookStrategy = require("passport-outlook").Strategy;
require("dotenv").config();

passport.use(
  new GoogleStrategy(
    {
      clientID: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
      callbackURL: "/auth/google/callback",
      scope: ["profile", "email"],
    },
    async (accessToken, refreshToken, profile, cb) => {
      try {
        const { displayName, emails, photos } = profile;
        const userDb = await User.findOne({ email: emails[0].value });

        if (!userDb) {
          await User.create({
            displayName: displayName,
            email: emails[0].value,
            avatar: photos[0].value,
            typeOauth: [{ type: "google" }],
            isActive: true,
          });
        }

        if (
          userDb &&
          !userDb.typeOauth.some((oauth) => oauth.type === "google")
        ) {
          const typeOauth = [...userDb.typeOauth, { type: "google" }];
          await User.updateOne({ email: emails[0].value }).set({
            typeOauth,
            avatar: photos[0].value,
          });
        }

        return cb(null, { email: emails[0].value });
      } catch (error) {
        return cb(true, error);
      }
    }
  )
);

passport.use(
  new OutlookStrategy(
    {
      clientID: process.env.OUTLOOK_CLIENT_ID,
      clientSecret: process.env.OUTLOOK_CLIENT_SECRET,
      callbackURL: "/auth/outlook/callback",
      scope: ["openid", "profile"],
    },
    async (accessToken, refreshToken, profile, cb) => {
      try {
        console.log(profile);
        const { displayName, emails, photos } = profile;
        const userDb = await User.findOne({ email: emails[0].value });

        if (!userDb) {
          await User.create({
            displayName: displayName,
            email: emails[0].value,
            avatar: photos[0].value,
            typeOauth: [{ type: "outlook" }],
            isActive: true,
          });
        }

        if (
          userDb &&
          !userDb.typeOauth.some((oauth) => oauth.type === "outlook")
        ) {
          const typeOauth = [...userDb.typeOauth, { type: "outlook" }];
          await User.updateOne({ email: emails[0].value }).set({ typeOauth });
        }

        return cb(null, { email: emails[0].value });
      } catch (error) {
        console.log(error);
        return cb(true, error);
      }
    }
  )
);
