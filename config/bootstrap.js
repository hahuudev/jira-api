/**
 * Seed Function
 * (sails.config.bootstrap)
 *
 * A function that runs just before your Sails app gets lifted.
 * > Need more flexibility?  You can also create a hook.
 *
 * For more information on seeding your app with fake data, check out:
 * https://sailsjs.com/config/bootstrap
 */

module.exports.bootstrap = async function (done) {
  // eslint-disable-next-line handle-callback-err
  Otp.native((err, collection) => {
    // define index properties
    collection.ensureIndex(
      { createdAt: 1 },
      { expireAfterSeconds: 2 * 60 * 60 }
    );

    // be sure to call the bootstrap callback to indicate completion
    done();
  });
};
