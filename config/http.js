const passport = require("passport");

module.exports.http = {
  middleware: {
    passportInit: passport.initialize(),
    // cors: cors({
    //   origin: ["http://localhost:3000"],
    //   credentials: true,
    // }),
    order: ["cookieParser", "passportInit", "bodyParser"],
  },
};
