/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {
  "*": "isLogged",

  "issue/update": ["isLogged", "isOwner"],
  "issue/delete": ["isLogged", "isOwner"],

  "project/update": ["isLogged", "isOwner"],
  "project/delete": ["isLogged", "isOwner"],
  "project/getById": ["isLogged", "isOwnerProject"],

  "board/update": ["isLogged", "isOwner"],
  "board/delete": ["isLogged", "isOwner"],

  "auth/*": true,
};
