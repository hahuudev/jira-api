/**
 * Security Settings
 * (sails.config.security)
 *
 * These settings affect aspects of your app's security, such
 * as how it deals with cross-origin requests (CORS) and which
 * routes require a CSRF token to be included with the request.
 *
 * For an overview of how Sails handles security, see:
 * https://sailsjs.com/documentation/concepts/security
 *
 * For additional options and more information, see:
 * https://sailsjs.com/config/security
 */

module.exports.security = {
  cors: {
    allRoutes: true,
    allowOrigins: ["http://localhost:3000", "https://jira-ui-nine.vercel.app"],
    allowCredentials: true,
    withCredentials: true,
    allowMethods: "GET, POST, PUT, DELETE, OPTIONS, HEAD",
    allowHeaders: "authorization,content-type",
    allowRequestHeaders: "authorization,content-type",
    allowResponseHeaders: "location,access-control-allow-origin",
  },
};
