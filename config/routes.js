/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

const passport = require("passport");
const AuthController = require("../api/controllers/AuthController");
const UserValidator = require("../api/validators/UserValidator");
const isLogin = require("../api/policies/isLogged");

module.exports.routes = {
  // Authenticate
  "POST /auth/login": [UserValidator.auth, AuthController.login],
  "POST /auth/send-otp": [AuthController.sendOtp],
  "POST /auth/register": [UserValidator.auth, AuthController.register],
  "GET /auth/forgot-pass": [
    UserValidator.sendMailForgotPass,
    AuthController.sendMailforgotPass,
  ],
  "GET /auth/forgot-pass/verify": [AuthController.verifyForgotpass],
  "POST /auth/forgot-pass/action": [
    UserValidator.forgotPass,
    AuthController.forgotPass,
  ],
  "GET /auth/google": [passport.authenticate("google")],
  "GET /auth/google/callback": [AuthController.google],
  "GET /auth/outlook": [passport.authenticate("windowslive")],
  "GET /auth/outlook/callback": [AuthController.outlook],
  "GET /auth/current-user": [isLogin, AuthController.getCurrentUser],
  "GET /auth/logout": [isLogin, AuthController.logout],
  "GET /auth/test": [AuthController.testSendMail],

  // User
  "GET /api/users": { action: "User/getAllUser" },
  "GET /api/users/:id": { action: "User/getById" },

  // Category Project
  "GET /api/category-project": {
    action: "categoryproject/find",
    skipAssets: true,
  },

  // Projecr
  "GET /api/projects": { action: "Project/getAllProject" },
  "GET /api/projects/:id": { action: "Project/getById" },

  // Issue
  "GET /api/issues": { action: "Issue/getAllIssueByProjectId" },
  "PATCH /api/issues": { action: "Issue/updateMutipleIssue" },

  // EMAIL
  "POST /api/email/invite-user": { action: "EmailTemplate/sendInviteUser" },

  // TEST
  "GET /api/test": { action: "Issue/test" },
};
