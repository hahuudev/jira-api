module.exports = async (req, res, next) => {
  try {
    const user = req.user;

    if (!user) {
      return res.status(400).json({
        message: "Authenticate fail",
      });
    }

    const id = req.params.id;
    const project = await Project.findOne({ id });

    if (!project) {
      return res.status(400).json({
        message: "Forbiden !!",
      });
    }

    if (project.userIds.every((item) => item !== user.id)) {
      return res.status(400).json({
        message: "Forbiden !!",
      });
    }

    return next();
  } catch (error) {
    return next(error.message);
  }
};
