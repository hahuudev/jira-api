const jwt = require("jsonwebtoken");
require("dotenv").config();

module.exports = async (req, res, next) => {
  // console.log(req.cookies);
  const bearerToken = req.headers.authorization;
  // const token = req.cookies.token;
  if (!bearerToken) {
    return res.status(401).json({ message: "You are not logged in" });
  }

  const token = bearerToken.split(" ")[1];

  jwt.verify(token, process.env.SECRET_KEY, async (error, payload) => {
    if (error) {
      return res.status(401).json(error);
    }

    const { id, email, displayName, role } = payload;
    const userDb = await User.findOne({ id, isActive: true });

    if (!userDb) {
      return res.status(400).json({
        message: "Authenticate fail. No user find in system",
      });
    }
    req.user = { id, displayName, email, role, avatar: userDb.avatar };

    return next();
  });
};


