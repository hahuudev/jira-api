module.exports = async (req, res, next) => {
  const user = req.user;

  if (!user || !user.role === "admin") {
    return res.status(400).json({
      message: "Authenticate fail",
    });
  }

  return next();
};
