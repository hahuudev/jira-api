module.exports = async (req, res, next) => {
  try {
    const user = req.user;

    if (!user) {
      return res.status(400).json({
        message: "Authenticate fail",
      });
    }

    const Model = req._sails.models[req.options.model];

    const id = req.params.id;

    if (req.options.model !== "project") {
      const record = await Model.findOne({ id: id });

      if (!record) {
        return res.status(400).json({
          message: "Forbiden !!",
        });
      }


      const project = await Project.findOne({ id: record.projectId });

      if (!project) {
        return res.status(400).json({
          message: "Forbiden !!",
        });
      }

      if (project.userIds.every((item) => item !== user.id)) {
        return res.status(400).json({
          message: "Forbiden !!",
        });
      }

      return next();
    }

    if (req.options.model === "project") {
      const project = await Project.findOne({ id });

      if (!project) {
        return res.status(400).json({
          message: "Forbiden !!",
        });
      }

      if (project.userIds.every((item) => item !== user.id)) {
        return res.status(400).json({
          message: "Forbiden !!",
        });
      }

      return next();
    }
  } catch (error) {
    return next(error.message);
  }
};
