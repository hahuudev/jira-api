/**
 * ProjectController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const joi = require("joi");

const schema = joi.object({
  name: joi.string().required(),
});

module.exports = async (req, res, next) => {
  try {
    const result = schema.validate(req.body);
    if (result.error) {
      return res.status(400).json({ message: result.error.details[0].message });
    }
    return next();
  } catch (error) {
    return res
      .status(500)
      .json({ error: "InternalServerError", message: error.message });
  }
};
