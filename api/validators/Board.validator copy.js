const Joi = require("joi");

const schema = Joi.object({
  title: Joi.string().min(10).max(255).required(),
  description: Joi.string().min(10).required(),
  projectId: Joi.string().required(),
  taskIds: Joi.array().items(Joi.string()),
});

module.exports = async (req, res, next) => {
  try {
    const resultValidate = schema.validate(req.body);
    if (resultValidate.error) {
      return res.status(400).json({ message: resultValidate.error.details[0].message });
    }
    return next();
  } catch (error) {
    return res
      .status(500)
      .json({ error: "InternalServerError", message: error.message });
  }
};
