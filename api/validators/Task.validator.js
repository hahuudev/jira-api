const Joi = require("joi");

const schema = Joi.object({
  title: Joi.string().min(10).max(255).required(),
  sumary: Joi.string().required(),
  description: Joi.string().min(10).required(),
  reporterId: Joi.string().required(),
  assignIds: Joi.array(),
  priority: Joi.string(),
  originalEstimate: Joi.number(),
  timeTracking: Joi.number().max(Joi.ref("originalEstimate")),
  boardId: Joi.string().required(),
});
const schemaField = Joi.object({
  title: Joi.string().min(10).max(255),
  sumary: Joi.string(),
  description: Joi.string().min(10),
  reporterId: Joi.string(),
  assignIds: Joi.array(),
  priority: Joi.string(),
  originalEstimate: Joi.number(),
  status: Joi.string(),
  timeTracking: Joi.number().max(Joi.ref("originalEstimate")),
});

module.exports = {
  validate: async (req, res, next) => {
    try {
      const resultValidate = schema.validate(req.body);
      if (resultValidate.error) {
        return res
          .status(400)
          .json({ message: resultValidate.error.details[0].message });
      }
      return next();
    } catch (error) {
      return res
        .status(500)
        .json({ error: "InternalServerError", message: error.message });
    }
  },
  validateField: async (req, res, next) => {
    try {
      const resultValidate = schemaField.validate(req.body);
      if (resultValidate.error) {
        return res
          .status(400)
          .json({ message: resultValidate.error.details[0].message });
      }
      return next();
    } catch (error) {
      return res
        .status(500)
        .json({ error: "InternalServerError", message: error.message });
    }
  },
};
