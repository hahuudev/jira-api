/**
 * ProjectController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const Joi = require("joi");
const httpError = require("../utils/httpError");

const schema = Joi.object({
  displayName: Joi.string(),
  email: Joi.string().email().required(),
  password: Joi.string().required(),
  otp: Joi.string(),
});

module.exports = {
  auth: async (req, res, next) => {
    try {
      const resultValidate = schema.validate(req.body);
      if (resultValidate.error) {
        return res
          .status(400)
          .json({ message: resultValidate.error.details[0].message });
      }
      return next();
    } catch (error) {
      return res
        .status(500)
        .json({ error: "InternalServerError", message: error.message });
    }
  },
  forgotPass: async (req, res, next) => {
    try {
      const schema = Joi.object({
        password: Joi.string().min(6).required(),
        confirmPassword: Joi.string().required().valid(Joi.ref("password")),
      });
      const resultValidate = schema.validate(req.body);
      if (resultValidate.error) {
        return res
          .status(400)
          .json({ message: resultValidate.error.details[0].message });
      }
      return next();
    } catch (error) {
      return res
        .status(500)
        .json({ error: "InternalServerError", message: error.message });
    }
  },
  sendMailForgotPass: async (req, res, next) => {
    try {
      const schema = Joi.object({
        email: Joi.string().email().required(),
      }).messages({
        "string.email": "Please enter a valid email address",
        "any.required": "Email query parameter is required",
      });
      const resultValidate = schema.validate(req.query);
      if (resultValidate.error) {
        return res
          .status(400)
          .json({ message: resultValidate.error.details[0].message });
      }
      return next();
    } catch (error) {
      return res
        .status(500)
        .json({ error: "InternalServerError", message: error.message });
    }
  },
};
