/**
 * ProjectController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  getAllIssueByProjectId: async (req, res) => {
    const { projectId, sort, assignIds, q, order, ...query } = req.query;
    const db = sails.getDatastore().manager;

    try {
      const filter = {
        ...query,
        projectId,
      };

      if (assignIds) {
        filter.assignIds = { $in: Array.from(assignIds.split(",")) };
      }

      if (q) {
        filter.title = { $regex: `.*${q}.*` };
      }

      const issuesAssign = await db
        .collection("issue")
        .aggregate([
          {
            $match: filter,
          },
          { $unwind: "$assignIds" },
          { $addFields: { idUser: { $toObjectId: "$assignIds" } } },
          {
            $lookup: {
              from: "user",
              localField: "idUser",
              foreignField: "_id",
              as: "assigns",
            },
          },

          {
            $group: {
              _id: "$_id",
              assigns: { $push: { $arrayElemAt: ["$assigns", 0] } },
              title: { $first: "$title" },
              type: { $first: "$type" },
              createdAt: { $first: "$createdAt" },
              updatedAt: { $first: "$updatedAt" },
              description: { $first: "$description" },
              projectId: { $first: "$projectId" },
              originalEstimate: { $first: "$originalEstimate" },
              timeTracking: { $first: "$timeTracking" },
              priority: { $first: "$priority" },
              columnId: { $first: "$columnId" },
              reporterId: { $first: "$reporterId" },
              position: { $first: "$position" },
              assignIds: { $push: "$idUser" },
            },
          },
          {
            $project: {
              _id: 0,
              id: "$_id",
              title: 1,
              type: 1,
              position: 1,
              assignIds: 1,
              createdAt: 1,
              updatedAt: 1,
              projectId: 1,
              originalEstimate: 1,
              timeTracking: 1,
              description: 1,
              reporterId: 1,
              priority: 1,
              columnId: 1,
              assigns: {
                $map: {
                  input: "$assigns",
                  as: "assign",
                  in: {
                    id: "$$assign._id",
                    avatar: "$$assign.avatar",
                    displayName: "$$assign.displayName",
                  },
                },
              },
            },
          },

          {
            $sort: sort
              ? { [sort]: +order || -1 }
              : {
                  createdAt: 1,
                },
          },
        ])
        .toArray();

      return res.status(200).json({
        error: 0,
        data: issuesAssign,
      });
    } catch (error) {
      return res
        .status(500)
        .json({ error: "InternalServerError", message: error.message });
    }
  },
  updateMutipleIssue: async (req, res) => {
    try {
      await Issue.update({ columnId: req.body.columnId }).set({
        columnId: req.body.newColumnId,
      });
      res.status(201).json({ message: "Update mutiple column successfully" });
    } catch (error) {
      res
        .status(500)
        .json({ error: "InternalServerError", message: error.message });
    }
  },
};
