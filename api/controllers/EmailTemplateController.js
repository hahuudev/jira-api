const httpError = require("../utils/httpError");
const sendEmail = require("../utils/sendEmail");
const ejs = require("ejs");
const Joi = require("joi");

module.exports = {
  sendInviteUser: async (req, res, next) => {
    try {
      const schema = Joi.object({
        email: Joi.string().email().required(),
        displayName: Joi.string().required(),
      });
      const resultValidate = schema.validate(req.body);
      if (resultValidate.error) {
        return res
          .status(400)
          .json({ message: result.error.details[0].message });
      }
      const { email } = req.body;

      const template = await EmailTemplate.findOne({
        slug: "template-invite-user",
      });

      const html = ejs.render(template.html, req.body);

      sendEmail(template.title, html, { email: email });

      res.status(200).json({ message: "Send invite successfully" });
    } catch (error) {
      return res
        .status(500)
        .json({ error: "InternalServerError", message: error.message });
    }
  },

  test: async (req, res, next) => {
    try {
      await EmailTemplate.create({
        name: "Send invitations to join project",
        title: "Admin is waiting for you to join them",
        slug: "template-invite-user",
        html: `<div
        style="
          font-family: Helvetica, Arial, sans-serif;
          min-width: 1000px;
          overflow: auto;
          line-height: 2;
        "
      >
        <div style="margin: 50px auto; width: 70%; padding: 20px 0">
          <h1>Your team is waiting for you to join them</h1>
          <div style="border-bottom: 1px solid #eee">
            <a
              href=""
              style="
                font-size: 1.4em;
                color: #00466a;
                text-decoration: none;
                font-weight: 600;
              "
              >Dear, <%= name %></a
            >
          </div>
          <p style="font-size: 16px; color: #030404">
            Admin <%= name %> has invited you to collaborate on hahuu.atlassian.net
            Join the team
          </p>
      
          <a href="<%=link %>" >Join the team</a>
          <p style="font-size: 0.9em">Regards,<br />Your Brand</p>
          <hr style="border: none; border-top: 1px solid #eee" />
          <p style="font-size: 14px; color: #aaa">
            Jira sẽ không bao giờ gửi email cho bạn và yêu cầu bạn tiết lộ hoặc xác
            minh mật khẩu, thẻ tín dụng hoặc số tài khoản ngân hàng của bạn.
          </p>
          <hr style="border: none; border-top: 1px solid #eee" />
          <div
            style="
              float: right;
              padding: 8px 0;
              color: #aaa;
              font-size: 0.8em;
              line-height: 1;
              font-weight: 300;
            "
          >
            <p>Your Brand Inc</p>
            <p>12345 Amphitheatre Parkway</p>
            <p>HAHUUDEV</p>
          </div>
        </div>
      </div>
      `,
      });
    } catch (error) {
      return res
        .status(500)
        .json({ error: "InternalServerError", message: error.message });
    }
  },
};
