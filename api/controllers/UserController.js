module.exports = {
  getAllUser: async (req, res) => {
    try {
      const {
        page = 1,
        limit = 10,
        search,
        fieldSearch = "name",
        sort = "createdAt",
        order = 1,
        ...query
      } = req.query;
      const skip = +limit * (page - 1);

      const filter = { ...query };

      if (search) {
        filter[fieldSearch] = { $regex: `.*${search}.*`, $options: "i" };
      }

      const db = await sails.getDatastore().manager;
      const UserModel = db.collection("user");

      const pipelineUser = [
        { $match: filter },
        { $project: { columnIds: 0 } },
        { $skip: skip },
        { $limit: +limit },
        { $addFields: { id: "$_id" } },
        { $project: { _id: 0 } },
        { $sort: { [sort]: +order } },
      ];

      const [users, totalUser] = await Promise.all([
        UserModel.aggregate(pipelineUser).toArray(),
        UserModel.countDocuments(),
      ]);

      return res.status(200).json({
        totalPage: Math.ceil(totalUser / limit),
        data: users,
        currentPage: page,
        count: totalUser,
      });
    } catch (error) {
      res.status(500).json({ error: true, message: error.message });
    }
  },
  getById: async (req, res) => {
    try {
      const user = await User.findOne({ id: req.params.id });
      return res.status(200).json({ error: 0, data: user });
    } catch (error) {
      res.status(500).json({ error: true, message: error.message });
    }
  },
};
