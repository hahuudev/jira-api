/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const httpError = require("../utils/httpError");
const bcrypt = require("bcrypt");
const sendEmail = require("../utils/sendEmail");
const generateToken = require("../utils/generateToken");
const jwt = require("jsonwebtoken");
const passport = require("passport");
const otpGenerator = require("otp-generator");
const ejs = require("ejs");

module.exports = {
  getCurrentUser: async (req, res, next) => {
    try {
      if (req.user) {
        // const { id } = req.user;
        // const userDb = await User.findOne({ id });

        if (!req.user) {
          return res.status(400).json({
            message: "Authenticate fail",
            info: null,
          });
        }
        res.status(200).json({
          message: "Authorization successfully",
          info: {
            id: req.user.id,
            displayName: req.user.displayName,
            email: req.user.email,
            avatar: req.user.avatar,
            role: req.user.role,
          },
        });
      } else {
        res.json({
          message: "Authenticate fail",
          info: null,
        });
      }
    } catch (error) {
      return res
        .status(500)
        .json({ error: "InternalServerError", message: error.message });
    }
  },

  login: async (req, res, next) => {
    try {
      const { email, password } = req.body;

      const userDb = await User.findOne({ email });

      if (!userDb) {
        return res
          .status(400)
          .json({ error: true, message: "Email is not registere in system" });
      }

      if (!userDb.isActive) {
        return res
          .status(400)
          .json({ message: "Email is not active in system" });
      }

      if (userDb.typeOauth.every((item) => item.type !== "local")) {
        return res
          .status(400)
          .json({ message: "The account has not been linked before" });
      }
      const isMatch = await bcrypt.compare(password, userDb.password);

      if (!isMatch) {
        return res
          .status(400)
          .json({ error: true, message: "Invalid displayName or password" });
      }

      const token = generateToken({
        id: userDb.id,
        role: userDb.role,
        email: userDb.email,
      });
      // return res.status(200).json({ message: "Login successfully", token });
      return (
        res
          // .cookie("token", token, {
          //   maxAge: 24 * 60 * 60 * 60 * 1000,
          //   httpOnly: true,
          // })
          .json({ message: "Login successfully", token })
      );
      // .redirect(process.env.URI_FE);
    } catch (error) {
      return res
        .status(500)
        .json({ error: "InternalServerError", message: error.message });
    }
  },

  sendOtp: async (req, res, next) => {
    try {
      const { email, displayName, ...obj } = req.body;

      if (!email) {
        return res.status(400).json({ message: "Email invalid" });
      }

      const userDb = await User.findOne({ email });

      if (userDb) {
        return res
          .status(400)
          .json({ error: true, message: "Email already exists in the system" });
      }

      const otp = otpGenerator.generate(6, {
        upperCaseAlphabets: false,
        specialChars: false,
        digits: true,
        lowerCaseAlphabets: false,
      });

      Otp.create({ email: email, otp }).then(async () => {
        const template = await EmailTemplate.findOne({ slug: "template-otp" });

        const html = ejs.render(template.html, {
          name: displayName,
          otp,
          ...obj,
        });
        // Send token to email register
        sendEmail(template.title, html, { email });
      });

      return res.status(201).json({ message: "Send otp successfully !!" });
    } catch (error) {
      return res
        .status(500)
        .json({ error: "InternalServerError", message: error.message });
    }
  },

  register: async (req, res, next) => {
    try {
      const { email, otp, password, displayName } = req.body;

      const otps = await Otp.find({ email }).sort("createdAt DESC");

      if (!otps || otps.length === 0) {
        return res.status(400).json({ message: "OTP not found" });
      }

      if (otp !== otps[0].otp) {
        return res.status(400).json({ message: "Invalid otp" });
      }

      if (otps[0].expiredAt < Date.now()) {
        return res.status(400).json({ message: "Otp expried" });
      }

      const userDb = await User.findOne({ email });

      if (userDb) {
        return res
          .status(400)
          .json({ error: true, message: "Email already exists in the system" });
      }

      // generate salt to hash
      const salt = await bcrypt.genSalt(10);

      const passwordHash = await bcrypt.hash(password, salt);

      await User.create({
        displayName,
        email,
        password: passwordHash,
        typeOauth: [{ type: "local" }],
        isActive: true,
      });

      // return res.redirect(`${process.env.URI_FE}/login?verify=true`);

      return res.status(201).json({ message: "Register successfully !!" });
    } catch (error) {
      return res
        .status(500)
        .json({ error: "InternalServerError", message: error.message });
    }
  },

  google: async (req, res, next) => {
    try {
      passport.authenticate(
        "google",
        { session: false },
        async (_err, user, info) => {
          if (!user) {
            return next(httpError.InternalServerError("Login faill"));
          }
          const userDb = await User.findOne({ email: user.email });
          const token = generateToken({
            id: userDb.id,
            role: userDb.role,
            email: userDb.email,
          });
          // return res.status(200).json({ message: "Login successfully", token });
          return (
            res
              // .cookie("token", token, {
              //   maxAge: 24 * 60 * 60 * 60 * 1000,
              //   httpOnly: true,
              // })
              .redirect(process.env.URI_FE + "?token=" + token)
          );

          // MAX 40;
        }
      )(req, res, next);
    } catch (error) {
      return res
        .status(500)
        .json({ error: "InternalServerError", message: error.message });
    }
  },

  outlook: async (req, res, next) => {
    try {
      passport.authenticate(
        "windowslive",
        { session: false },
        async (_err, user, info) => {
          if (!user) {
            return next(httpError.InternalServerError("Login fail"));
          }
          const userDb = await User.findOne({ email: user.email });
          const token = generateToken({
            id: userDb.id,
            role: userDb.role,
            email: userDb.email,
          });
          // return res.status(200).json({ message: "Login successfully", token });
          return res
            .cookie("token", token, {
              maxAge: 24 * 60 * 60 * 60 * 1000,
              httpOnly: true,
            })
            .redirect(process.env.URI_FE);
        }
      )(req, res, next);
    } catch (error) {
      return res
        .status(500)
        .json({ error: "InternalServerError", message: error.message });
    }
  },

  sendMailforgotPass: async (req, res, next) => {
    try {
      const email = req.query.email;

      const userDb = await User.findOne({ email });

      if (!userDb) {
        return res
          .status(400)
          .json({ error: true, message: "Email is not registere in system" });
      }

      const tokenVerify = generateToken({ email, type: "verify-pass" });

      const html = `<div>
      <h1>Please reset your password</h1> 
      <p>Vui lòng nhấp link bên dưới để forgo pass</p>
      <a href="http://localhost:1337/auth/forgot-pass/verify?verifyPass_token=${tokenVerify}">http://localhost:1337/auth/forgot-pass/verify?verifyPass_token=${tokenVerify}</a>
      </div>`;

      // Send token to email forgot-pass
      sendEmail("Please reset your password", html, { email, tokenVerify });

      res.status(200).json({ message: "Send token to email successfully" });
    } catch (error) {
      return res
        .status(500)
        .json({ error: "InternalServerError", message: error.message });
    }
  },

  verifyForgotpass: async (req, res, next) => {
    try {
      const token = req.query.verifyPass_token;
      jwt.verify(token, process.env.SECRET_KEY, async (error, payload) => {
        if (error) {
          res.redirect(`${process.env.URI_FE}/verify-pass?verify=false`);
        }

        if (payload && payload.type !== "verify-pass") {
          return res.status(400).json({ message: "Token invalid" });
        }

        const userDb = await User.findOne({
          email: payload.email,
        });

        if (userDb) {
          return res.view("forgot-pass");
        }
        return res.status(404).json({ message: "Not found" });
      });
    } catch (error) {
      return res
        .status(500)
        .json({ error: "InternalServerError", message: error.message });
    }
  },
  forgotPass: async (req, res, next) => {
    const { password } = req.body;

    // try {
    //   const token = req.query.verifyPass_token;
    //   jwt.verify(token, process.env.SECRET_KEY, async (error, payload) => {
    //     if (error) {
    //       res.redirect(`${process.env.URI_FE}/verify-pass?verify=false`);
    //     }

    //     if (payload && payload.type !== "verify-pass") {
    //       return res.status(400).json({ message: "Token invalid" });
    //     }

    //     const salt = await bcrypt.genSalt(10);

    //     const passwordHash = await bcrypt.hash(password, salt);

    //     const userDb = await User.updateOne({
    //       email: payload.email,
    //     }).set({ isActive: true, password: passwordHash, tokenVerify: "" });

    //     if (userDb && userDb.typeOauth.some((item) => item.type !== "local")) {
    //       const typeOauth = [...userDb.typeOauth, { type: "local" }];
    //       await User.updateOne({
    //         email: payload.email,
    //       }).set({ typeOauth });

    //       return res.redirect(`${process.env.URI_FE}/login`);
    //     }

    //     return res.status(404).json({ message: "Not found" });
    //   });
    // } catch (error) {
    //   return res.status(500).json({error:'InternalServerError', message: error.message})
    // }
  },

  logout: async (req, res, next) => {
    try {
      res
        .status(200)
        .clearCookie("token")
        .json({ message: "Logout successfully" });
    } catch (error) {
      return res
        .status(500)
        .json({ error: "InternalServerError", message: error.message });
    }
  },

  testSendMail: async (req, res, next) => {
    try {
    } catch (error) {
      console.log(error);
    }
  },
};
