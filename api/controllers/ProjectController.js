// /**
//  * ProjectController
//  *
//  * @description :: Server-side actions for handling incoming requests.
//  * @help        :: See https://sailsjs.com/docs/concepts/actions
//  */

module.exports = {
  getAllProject: async (req, res) => {
    try {
      const {
        page = 1,
        limit = 10,
        search,
        fieldSearch = "name",
        sort = "createdAt",
        order = 1,
        ...query
      } = req.query;
      const skip = +limit * (page - 1);

      const filter = { ...query };

      if (search) {
        filter[fieldSearch] = { $regex: `.*${search}.*`, $options: "i" };
      }

      const db = await sails.getDatastore().manager;
      const ProjectModel = db.collection("project");

      const pipelinePrj = [
        { $match: filter },
        { $project: { columnIds: 0 } },
        { $sort: { [sort]: +order } },
        { $skip: skip },
        { $limit: +limit },
        { $addFields: { id: "$_id" } },
        { $project: { _id: 0 } },
      ];

      const [projects, totalProject] = await Promise.all([
        ProjectModel.aggregate(pipelinePrj).toArray(),
        ProjectModel.countDocuments(),
      ]);

      return res.status(200).json({
        currentPage: page,
        data: projects,
        totalPage: Math.ceil(totalProject / limit),
        count: totalProject,
      });
    } catch (error) {
      res.status(500).json({ error: true, message: error.message });
    }
  },

  getById: async (req, res) => {
    try {
      const [res1, res2] = await Promise.all([
        Project.findOne({ id: req.params.id }),
        Column.find({ projectId: req.params.id }),
      ]);

      const { columnIds: _, ...objRes1 } = res1;

      return res.status(200).json({
        error: 0,
        data: { ...objRes1, columns: res2 },
      });
    } catch (error) {
      return res
        .status(500)
        .json({ error: "InternalServerError", message: error.message });
    }
  },
};
