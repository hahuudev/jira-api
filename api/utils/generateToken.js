const jwt = require("jsonwebtoken");
require("dotenv").config();
const CryptoJS = require("crypto-js");

module.exports = (data, expiresIn) => {
  const token = jwt.sign(data, process.env.SECRET_KEY, {
    expiresIn: expiresIn || "1d",
  });
  return token;
};

// module.exports = {
//   sign: (data, expiresIn) => {
//     const secretKey = "secretKey";

//     const encodedHeader = btoa(JSON.stringify({ alg: "HS256", typ: "JWT" }))
//       .replace(/\+/g, "-")
//       .replace(/\//g, "_")
//       .replace(/=+$/, "");

//     const encodedPayload = btoa(JSON.stringify(data))
//       .replace(/\+/g, "-")
//       .replace(/\//g, "_")
//       .replace(/=+$/, "");

//     const signature = btoa(
//       CryptoJS.HmacSHA256(`${encodedHeader}.${encodedPayload}`, secretKey)
//     )
//       .replace(/\+/g, "-")
//       .replace(/\//g, "_")
//       .replace(/=+$/, "");

//     const token = `${encodedHeader}.${encodedPayload}.${signature}`;

//     return token;
//   },

//   verify: () => {
//     const decodeToken = (token, secretKey) => {
//       const [encodedHeader, encodedPayload, signature] = token.split(".");

//       const decodedHeader = atob(
//         encodedHeader.replace(/-/g, "+").replace(/_/g, "/")
//       );
//       const decodedPayload = atob(
//         encodedPayload.replace(/-/g, "+").replace(/_/g, "/")
//       );

//       const header = JSON.parse(decodedHeader);
//       const payload = JSON.parse(decodedPayload);

//       const calculatedSignature = CryptoJS.HmacSHA256(
//         `${encodedHeader}.${encodedPayload}`,
//         secretKey
//       );
//       const calculatedSignatureBase64 = btoa(calculatedSignature);

//       if (signature !== calculatedSignatureBase64) {
//         throw new Error("Invalid token");
//       }

//       return payload;
//     };
//   },
// };
