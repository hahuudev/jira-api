const createError = require("http-errors");

const httpError = {
  NotFound: () => createError(404, "Router not found"),
  BadRequest: (err) => createError.BadRequest(err),
  InternalServerError: (err) => createError.InternalServerError(err),
};

module.exports = httpError;
