const nodemailer = require("nodemailer");
require("dotenv").config();

const transporter = nodemailer.createTransport({
  host: "smtp.gmail.com",
  port: 587,
  secure: false,
  auth: {
    user: process.env.ACCOUNT_EMAIL,
    pass: process.env.ACCOUNT_PASS,
  },
});
module.exports = async (title, html, data) => {
  try {
    const mainOptions = {
      from: {
        name: "Jira software clone",
        address: "hahuudev.online",
      },
      to: data.email,
      subject: title,
      html: html,
    };

    transporter.sendMail(mainOptions, (err, info) => {
      if (err) {
        console.log(err);
        return { err };
      } else {
        console.log("Message sent: " + info.response);
        return info;
      }
    });
  } catch (error) {
    console.log(error.message);
  }
};
