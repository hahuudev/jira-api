module.exports = {
  schema: true,
  attributes: {
    id: { type: "string", columnName: "_id" },
    title: { type: "string", minLength: 6, maxLength: 255 },
    projectId: { type: "string", required: true },
    type: {
      type: "string",
      isIn: ["task", "bug", "story"],
      defaultsTo: "task",
    },
    description: { type: "string", minLength: 10 },
    reporterId: { type: "string", required: true },
    assignIds: {
      type: "json",
      columnType: "array",
      defaultsTo: [],
    },

    originalEstimate: { type: "number", defaultsTo: -1 },
    timeTracking: { type: "number", defaultsTo: -1 },
    priority: {
      type: "string",
      isIn: ["hightest", "hight", "medium", "low", "lowest"],
      defaultsTo: "low",
    },
    position: { type: "number" },
    columnId: { model: "column", required: true },
  },
};
