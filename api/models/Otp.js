module.exports = {
  attributes: {
    id: { type: "string", columnName: "_id" },
    otp: {
      type: "string",
      minLength: 6,
      maxLength: 6,
      required: true,
    },
    email: { type: "string", required: true },
    expiredAt: {
      type: "number",
    },
  },

  beforeCreate: async function (values, cb) {
    values.expiredAt = 2 * 60 * 1000 + Date.now();
    values.createdAt = Date.now();
    values.updatedAt = Date.now();
    cb();
  },
};
