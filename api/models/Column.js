module.exports = {
  schema: true,
  attributes: {
    id: { type: "string", columnName: "_id" },
    name: { type: "string", maxLength: 200, required: true },
    description: { type: "string", maxLength: 200, required: true },
    position: { type: "number" },
    projectId: { model: "project", required: true },
  },
  afterCreate: async function (board, proceed) {
    const project = await Project.findOne({ id: board.projectId });

    const newColumnIds = [...project.columnIds, board.id];
    await Project.updateOne(
      { id: board.projectId },
      { columnIds: newColumnIds }
    );

    proceed();
  },
  afterDestroy: async function (board, proceed) {
    const project = await Project.findOne({ id: board.projectId });

    const newColumnIds = project.columnIds.filter(
      (columnId) => columnId !== board.id
    );

    await Project.updateOne(
      { id: board.projectId },
      { columnIds: newColumnIds }
    );

    proceed();
  },
};
