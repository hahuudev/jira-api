module.exports = {
  tablename: "user",
  schema: true,
  attributes: {
    id: { type: "string", columnName: "_id" },
    displayName: { type: "string", required: true },
    email: { type: "string", unique: true, required: true },
    password: { type: "string" },
    isActive: { type: "boolean", defaultsTo: false },
    // tokenVerify: { type: "string" },
    typeOauth: {
      type: "json",
      columnType: "array",
      defaultsTo: [{ type: "local" }],
    },
    avatar: { type: "string" },
    role: { type: "string", defaultsTo: "user" },
  },
};
