/**
 * Project.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  schema: true,
  attributes: {
    id: { type: "string", columnName: "_id" },
    name: { type: "string", required: true },
    url: { type: "string", required: true },
    sumary: { type: "string", minLength: 6, maxLength: 255, required: true },
    description: { type: "string", minLength: 10 },
    category: { type: "string", required: true },
    userIds: { type: "json", columnType: "array", required: true },
    columnIds: { type: "json", columnType: "array", defaultsTo: [] },
  },

  beforeDestroy: async function (project, cb) {
    await Column.destroy({ projectId: project.where.id });
    cb();
  },
};
