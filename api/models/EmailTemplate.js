module.exports = {
  attributes: {
    id: { type: "number", columnName: "_id", autoIncrement: true },
    name: { type: "string", required: true },
    title: { type: "string", required: true },
    slug: { type: "string", required: true, unique: true },
    html: { type: "string", required: true },
  },
  beforeCreate: async function (values, cb) {
    console.log(values);
    try {
      const res = await EmailTemplate.findOne({ slug: values.slug });
      if (res) {
        return cb("Unique slug field");
      }
      return cb();
    } catch (error) {
      return cb(error.message);
    }
  },
};
